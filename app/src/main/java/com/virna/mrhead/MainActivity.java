package com.virna.mrhead;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private ImageView eyebrowImageView;
    private ImageView hairImageView;
    private ImageView moustacheImageView;
    private ImageView beardImageView;
    private CheckBox eyebrowCheckbox;
    private CheckBox hairCheckbox;
    private CheckBox moustacheCheckbox;
    private CheckBox beardCheckbox;
    TextView textview, textView2;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        eyebrowImageView = findViewById(R.id.eyebrowImageView);
        hairImageView = findViewById(R.id.hairImageView);
        moustacheImageView = findViewById(R.id.moustacheImageView);
        beardImageView = findViewById(R.id.beardImageView);
        eyebrowCheckbox = findViewById(R.id.eyebrowCheckbox);
        hairCheckbox = findViewById(R.id.hairCheckbox);
        moustacheCheckbox = findViewById(R.id.moustacheCheckbox);
        beardCheckbox = findViewById(R.id.beardCheckbox);
        textview = findViewById(R.id.textView);
        textView2 = findViewById(R.id.textView);


        eyebrowCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    eyebrowImageView.setVisibility(View.VISIBLE);
                } else {
                    eyebrowImageView.setVisibility(View.GONE);
                }
            }
        });

        hairCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    hairImageView.setVisibility(View.VISIBLE);
                } else {
                    hairImageView.setVisibility(View.GONE);
                }
            }
        });

        moustacheCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    moustacheImageView.setVisibility(View.VISIBLE);
                } else {
                    moustacheImageView.setVisibility(View.GONE);
                }
            }
        });

        moustacheCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    moustacheImageView.setVisibility(View.VISIBLE);
                } else {
                    moustacheImageView.setVisibility(View.GONE);
                }
            }
        });

        beardCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    beardImageView.setVisibility(View.VISIBLE);
                } else {
                    beardImageView.setVisibility(View.GONE);
                }
            }
        });
    }
}